# Numerical Solutions of Third-Order Boundary Value Problems
========================================================================

In this project, the aim is to approximate the numerical solutions of the third-order boundary value problems using finite difference method. The main feature of the finite difference method is to obtain discrete equations by replacing derivatives with appropriate finite differences. In this work, finite difference method for the third-order boundary value problems have been derived in four steps. In the first step, we have discretized the domain of the problem. In the second step, we have discretized the differential equation at the interior nodal points. The third step is devoted to the implement of the boundary conditions. Finally, the resulting linear systems have been solved using Gaussian elimination method.

